let texto = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum. \nPhasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis. \nVestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit."

//Cria nova string vazia.
let novoTexto = "";

//Itera cada caractere da string texto.
for (carac = 0; carac < texto.length; carac++){
    //Declara variável 'novoCarac', atribuindo-lhe o enésimo caractere do texto. 
    let novoCarac = texto[carac];
    //Verifica os casos em que o novoCarac é igual ponto ou vírgula, atribuindo-lhe 0 e -1 respectivamente.
    switch (novoCarac){
        case ".":
            novoCarac = 0;
            break;
        case ",":
            novoCarac = -1;
            break;
        default:
            //Verifica se o caractere é diferente de espaço vazio. 
            if (novoCarac !=" "){
             //Converte novoCarac para maiúscula e seleciona seu valor na tabela ASCII. Como 'A' equivale a 65, subtrai 64 de seu valor para que o mesmo passe a valer 1; 'B', 2; 'C', 3; e assim por diante.
            novoCarac = novoCarac.toUpperCase().charCodeAt() - 64;
        }
    }
    //Converte o valor para string e adiciona ao novo texto.
    novoTexto += novoCarac.toString();
}

//Imprime o novo texto.
console.log(novoTexto);